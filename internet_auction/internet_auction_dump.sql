-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: internet_auction
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bids`
--

DROP TABLE IF EXISTS `bids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bids` (
  `bid_id` int(11) NOT NULL AUTO_INCREMENT,
  `bid_date` date DEFAULT NULL,
  `bid_value` double DEFAULT NULL,
  `items_item_id` int(11) NOT NULL,
  `users_user_id` int(11) NOT NULL,
  PRIMARY KEY (`bid_id`),
  KEY `fk_bids_items1_idx` (`items_item_id`),
  KEY `fk_bids_users1_idx` (`users_user_id`),
  CONSTRAINT `fk_bids_items1` FOREIGN KEY (`items_item_id`) REFERENCES `items` (`item_id`),
  CONSTRAINT `fk_bids_users1` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bids`
--

LOCK TABLES `bids` WRITE;
/*!40000 ALTER TABLE `bids` DISABLE KEYS */;
INSERT INTO `bids` VALUES (1,'2004-01-19',850,8,4),(2,'2003-01-19',1200,6,6),(3,'2005-01-19',190,7,3),(4,'2009-01-19',200,7,5),(5,'2031-12-19',900,2,3),(6,'2008-01-19',1300,1,4),(7,'2004-01-19',2470,3,7),(8,'2009-01-19',1290,4,9),(9,'2008-01-19',500,5,10),(10,'2007-01-19',790,8,1);
/*!40000 ALTER TABLE `bids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `start_price` double DEFAULT NULL,
  `bid_increment` double DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `stop_date` date DEFAULT NULL,
  `by_it_now` binary(1) DEFAULT NULL,
  `users_user_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `fk_items_users_idx` (`users_user_id`),
  CONSTRAINT `fk_items_users` FOREIGN KEY (`users_user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'Зев Малмортиуса','Зев Малмортиуса',850,50,'2019-01-01','2019-01-15',NULL,5),(2,'Астральный искатель войны','Астральный искатель войны',1200,100,'2018-12-20','2019-01-20',NULL,1),(3,'Недобрая мишень гнусности','Недобрая мишень гнусности',500,50,'2018-12-30','2019-01-05',NULL,2),(4,'Далекий таинственный посох','Далекий таинственный посох',1250,150,'2019-01-05','2019-01-29',NULL,6),(5,'Заразные стражники хребта','Заразные стражники хребта',1600,200,'2019-01-02','2019-01-19',NULL,7),(6,'Душащая бритва сквайра','Душащая бритва сквайра',2500,50,'2019-01-04','2019-01-19',NULL,3),(7,'Мусорная тяжелая дубина','Мусорная тяжелая дубина',100,10,'2018-12-18','2018-12-30',NULL,1),(8,'Кровопускательная головня','Кровопускательная головня',550,50,'2018-12-29','2019-02-19',NULL,4),(9,'Пронизывающие шёлковые сапоги','Пронизывающие шёлковые сапоги',420,45,'2019-01-01','2019-01-24',NULL,8),(10,'Медвежья магическая шляпа','Медвежья магическая шляпа',1320,180,'2019-01-07','2019-02-10',NULL,3);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(45) DEFAULT NULL,
  `billing_adress` varchar(100) DEFAULT NULL,
  `login` varchar(20) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Ахметзянова Татьяна','г. Колпна, ул. Кустанайская, дом 17, квартира 59','Ahmetzyanova125','xBSJ8s9FmBsa'),(2,'Секунова Владлена','г. Заливино, ул. Кисловский Б. Переулок, дом 36, квартира 8','SekunovaVladlena144','omdh8LHIkyBR'),(3,'Соколов Вышеслав','г. Первомайское, ул. Чоботовская 2-я Аллея, дом 14, квартира 1','SokolovVyisheslav310','XV0TCcUmwnkN'),(4,'Степанова Алиса','г. Хабаровск, ул. Гороховский Переулок, дом 13, квартира 131','StepanovaAlisa132','BY964SvTqeDh'),(5,'Макаров Тит','г. Лермонтов, ул. Щетининский Переулок, дом 40, квартира 207','MakarovTit154','JJICAUYuaZAY'),(6,'Фёдорова Дина','г. Ножай-Юрт, ул. Боткинский 2-й Проезд, дом 85, квартира 144','FedorovaDina355','6HNQXwh5AGwb'),(7,'Киселёв Виталий','г. Ольховатка, ул. Магаданская, дом 58, квартира 103','KiselevVitaliy378','exkT2qSF7YPn'),(8,'Покровский Тихон','г. Целина, ул. Константина Царева, дом 41, квартира 94','PokrovskiyTihon336','izC7rrzQL1rd'),(9,'Литвина Пульхерия ','г. Бутурлиновка, ул. Лихачевский 3-й Переулок, дом 46, квартира 170','LitvinaPulheriya46','56uKFk9RXc80'),(10,'Вирский Велимир','г. Варнавино, ул. Кожуховская 7-я, дом 90, квартира 63','VirskiyVelimir260','1Qp31kmMqoUn'),(11,'Морозова Луиза','г. Завьялово, ул. Саранская, дом 31, квартира 154','MorozovaLuiza79','OycFyLBEnx9M');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-18 18:18:20
