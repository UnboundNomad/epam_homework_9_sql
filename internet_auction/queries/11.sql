DELETE child
FROM bids AS child INNER JOIN items AS parent ON child.items_item_id = parent.item_id
WHERE parent.users_user_id = 3;

DELETE FROM items
WHERE users_user_id = 3