WITH search AS (
SELECT s.student_id
FROM student s INNER JOIN marks m ON m.student_id = s.student_id
WHERE mark = 2
GROUP BY s.student_id
HAVING count(mark) >= 3 )

DELETE m, s FROM marks m, student s
WHERE m.student_id IN (SELECT * FROM search) AND s.student_id = m.student_id
