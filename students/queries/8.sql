SELECT name
From marks m inner join student s on m.student_id = s.student_id
Where mark = 2
GROUP BY name
Having count(mark) = 2